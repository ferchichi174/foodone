@extends('welcome')
@section('carousel')
    <section class="banner-slider-area section-padding yellow-bg">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-md-12">
                    <div class="banner">
                        <h2>Food One</h2>
                        <p>Poke is diced raw fish served either as an appetizer or as a <br> main course and is one of the main dishes of Native <br> Hawaiian cuisine. Traditional forms are aku and heʻe.</p>

                        <div class="social">
                            <a href=""><i class="fab fa-facebook-square"></i></a>
                            <a href=""><i class="fab fa-instagram"></i></a>
                        </div>
                        <div class="pagename">
                            <span class="number">01</span>
                            <span class="name">Burger</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5 col-md-12">
                    <div class="banner">
                        <img src="{{ asset('assets/images/4-a.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="banner-slider-area section-padding yellow-bg-2">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-md-12">
                    <div class="banner">
                        <h2>Quality Belongs to Us !</h2>
                        <p>Poke is diced raw fish served either as an appetizer or as a <br> main course and is one of the main dishes of Native <br> Hawaiian cuisine. Traditional forms are aku and heʻe.</p>

                        <div class="social">
                            <a href=""><i class="fab fa-facebook-square"></i></a>
                            <a href=""><i class="fab fa-instagram"></i></a>
                        </div>
                        <div class="pagename">
                            <span class="number">02</span>
                            <span class="name">Pizza</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5 col-md-12">
                    <div class="banner">
                        <img src="{{ asset('assets/images/chaplureg.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="banner-slider-area section-padding blue-bg">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-md-12">
                    <div class="banner">
                        <h2>Hot Pasta you Love!</h2>
                        <p>Poke is diced raw fish served either as an appetizer or as a <br> main course and is one of the main dishes of Native <br> Hawaiian cuisine. Traditional forms are aku and heʻe.</p>

                        <div class="social">
                            <a href=""><i class="fab fa-facebook-square"></i></a>
                            <a href=""><i class="fab fa-instagram"></i></a>
                        </div>
                        <div class="pagename">
                            <span class="number">03</span>
                            <span class="name">Pasta</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5 col-md-12">
                    <div class="banner">
                        <img src="{{ asset('assets/images/10f.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <section class="section-padding-2">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 cl-black mb-30">
                    <div class="section-title">
                        <h4>QUI NOUS SOMMES</h4>
                        <h2>Bienvenue à Foodone</h2>
                    </div>
                    <div class="about-us-content">
                        <p>Spécialisée dans la production des feuilles de Brick (Malsouka), la société Food One a été créée en 2013, Elle commercialise ses produits sous la marque BRIKADOR,
                            Food One exporte environ 40% de sa production vers plus de 10 pays dans le monde, notamment, Algérie, Libye, Liban, France, Belgique, Allemagne, USA, Côte d`Ivoire, Suède etc..
                            La société Food One est implantée à Zaouiet Jedidi, à coté de la ville de Beni Khalled, Gouvernorat de Nabeul - Tunisie</p>
                        <a href="about.html" class="bttn-mid btn-fill">Explore more</a>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
                    <div class="about-img">
                        <img src="{{ asset('assets/images/slid.png') }}" alt="">
                        <a href="https://www.youtube.com/watch?v=DJyxwIGdl8Y" class="video-popup"><i class="fa fa-play"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/About Area-->

    <!--pack Area-->
    <section class="pack-section">
        <div class="single-pack one">
            <h3>Feuilles de brick</h3>
            <p>Placeat sit excepturi sapiente <br> reiciendis quo sed adip</p>
            <a href="shop.html" class="bttn-mid btn-emt">Voir Plus</a>
            <img src="{{ asset('assets/images/brik.jpg') }}" alt="">
        </div>
        <div class="single-pack two">
            <h3>Couscous</h3>
            <p>Placeat sit excepturi sapiente <br> reiciendis quo sed adip</p>
            <a href="shop.html" class="bttn-mid btn-emt">Voir Plus</a>
            <img src="{{ asset('assets/images/couscous.png') }}" alt="">
        </div>
        <div class="single-pack three">
            <h3>Chapelure</h3>
            <p>Placeat sit excepturi sapiente <br> reiciendis quo sed adip</p>
            <a href="shop.html" class="bttn-mid btn-emt">Voir Plus</a>
            <img src="{{ asset('assets/images/chape.jpg') }}" alt="">
        </div>
    </section><!--/pack Area-->

    <!--Why Choose Us-->
    <section class="section-padding-3">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8 centered">
                    <div class="section-title mb-60">
                        <h4>We're Special</h4>
                        <h2>Les Markes Foodone</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="single-why-choose">
                        <div class="icon">
                            <img src="{{ asset('assets/images/migalo.png') }}" alt="">
                        </div>
                        <div class="title">
                            <h3>Migalo</h3>
                        </div>
                        <p>Voluptatibus, fugiat. Perspiciatis libero saepe ullam rem qui</p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="single-why-choose">
                        <div class="icon">
                            <img src="{{ asset('assets/images/mima.png') }}" alt="">
                        </div>
                        <div class="title">
                            <h3>Mima</h3>
                        </div>
                        <p>Voluptatibus, fugiat. Perspiciatis libero saepe ullam rem qui</p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="single-why-choose">
                        <div class="icon">
                            <img src="{{ asset('assets/images/lolly.png') }}" alt="">
                        </div>
                        <div class="title">
                            <h3>Lolly</h3>
                        </div>
                        <p>Voluptatibus, fugiat. Perspiciatis libero saepe ullam rem qui</p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="single-why-choose">
                        <div class="icon">
                            <img src="{{ asset('assets/images/brikador.png') }}" alt="">
                        </div>
                        <div class="title">
                            <h3>Brickador</h3>
                        </div>
                        <p>Voluptatibus, fugiat. Perspiciatis libero saepe ullam rem qui</p>
                    </div>
                </div>

            </div>
        </div>
    </section><!--/Why Choose Us-->


    <!--Food Category-->
    <section class="section-padding flower">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-8 centered">
                    <div class="section-title">
                        <h4>All Foods</h4>
                        <h2>Les Produits Foodone</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <ul class="nav nav-pills mb-3 food-cat-tab" id="pills-tab" role="tablist">
                        <li class="nav-item single-food-category" role="presentation">
                            <a class="nav-link active" id="tab-one-tab" data-toggle="pill" href="#tab-one" role="tab" aria-controls="tab-one" aria-selected="true">
                                <img src="{{ asset('assets/images/10f.png') }}" alt="">
                                <span>Feuilles de Brick</span>
                            </a>
                        </li>
                        <li class="nav-item single-food-category" role="presentation">
                            <a class="nav-link" id="tab-two-tab" data-toggle="pill" href="#tab-two" role="tab" aria-controls="tab-two" aria-selected="false">
                                <img src="{{ asset('assets/images/aaaa3.jpg') }}" alt="">
                                <span>Couscous fais main</span>
                            </a>
                        </li>
                        <li class="nav-item single-food-category" role="presentation">
                            <a class="nav-link" id="tab-three-tab" data-toggle="pill" href="#tab-three" role="tab" aria-controls="tab-three" aria-selected="false">
                                <img src="{{ asset('assets/images/aaaa1.jpg') }}" alt="">
                                <span>Pates spécial</span>
                            </a>
                        </li>
                        <li class="nav-item single-food-category" role="presentation">
                            <a class="nav-link" id="tab-four-tab" data-toggle="pill" href="#tab-four" role="tab" aria-controls="tab-four" aria-selected="false">
                                <img src="{{ asset('assets/images/chaplureg.png') }}" alt="">
                                <span>Chapelure</span>
                            </a>
                        </li>
                        <li class="nav-item single-food-category" role="presentation">
                            <a class="nav-link" id="tab-five-tab" data-toggle="pill" href="#tab-five" role="tab" aria-controls="tab-five" aria-selected="false">
                                <img src="{{ asset('assets/images/kounafa.png') }}" alt="">
                                <span>Kunafa</span>
                            </a>
                        </li>
                        <li class="nav-item single-food-category" role="presentation">
                            <a class="nav-link" id="tab-six-tab" data-toggle="pill" href="#tab-six" role="tab" aria-controls="tab-six" aria-selected="false">
                                <img src="{{ asset('assets/images/lolly.png') }}" alt="">
                                <span>Sucette fait main</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="tab-one" role="tabpanel" aria-labelledby="tab-one-tab">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href="product-details.html"><img src="{{ asset('assets/images/5f.png') }}" alt=""></a>
                                            <div class="wishlist"><a href="" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-heart"></i></a></div>


                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href="category.html"><i class="fas fa-hamburger"></i>Burger</a>
                                                <a href="category.html"><i class="fas fa-utensils"></i>Kolas Food</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="product-details.html">Feuilles de brick Brickador Farine 10 feilles</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/10f.png') }}" alt=""></a>
                                            <div class="wishlist"><a href="" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-heart"></i></a></div>
                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href="category.html"><i class="fas fa-hamburger"></i>Burger</a>
                                                <a href="category.html"><i class="fas fa-utensils"></i>Kolas Food</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Feuilles De Brick Brickador Farine 10 Feilles</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/brikalis.png') }}" alt=""></a>
                                            <div class="wishlist"><a href=""><i class="fas fa-heart"></i></a></div>

                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href="category.html"><i class="fas fa-hamburger"></i>Burger</a>
                                                <a href="category.html"><i class="fas fa-utensils"></i>Kolas Food</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Feuilles De Brick La Flamme</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-two" role="tabpanel" aria-labelledby="tab-two-tab">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/aaaa2.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href=""><i class="fas fa-heart"></i></a></div>

                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-map-marker-alt"></i>Los Angels</a>
                                                <a href=""><i class="fas fa-utensils"></i>Kolas Food</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Couscous Fin</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/aaaa3.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href=""><i class="fas fa-heart"></i></a></div>
                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-map-marker-alt"></i>Los Angels</a>
                                                <a href=""><i class="fas fa-utensils"></i>Kolas Food</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Couscous Moyen</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/aaaa4.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href=""><i class="fas fa-heart"></i></a></div>
                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-map-marker-alt"></i>Los Angels</a>
                                                <a href=""><i class="fas fa-utensils"></i>Kolas Food</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Couscous Gros</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-three" role="tabpanel" aria-labelledby="tab-three-tab">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/items/17.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href="" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-heart"></i></a></div>


                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-hamburger"></i>Burger</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Samusage pasta lit</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/items/18.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href="" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-heart"></i></a></div>
                                            <div class="ratings">4.6</div>
                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-pizza-slice"></i>Pizza</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Samusage pasta lit</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                                <div class="price">$26.08 <del>$32.5</del></div>
                                                <a href="cart.html" class="bttn-small btn-fill">Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/items/19.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href=""><i class="fas fa-heart"></i></a></div>
                                            <div class="ratings">4.6</div>
                                            <div class="discount-rate">10% off</div>
                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-pizza-slice"></i>Pizza</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Samusage pasta lit</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                                <div class="price">$26.08 <del>$32.5</del></div>
                                                <a href="cart.html" class="bttn-small btn-fill">Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/items/20.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href=""><i class="fas fa-heart"></i></a></div>
                                            <div class="ratings">4.6</div>
                                            <div class="discount-rate">10% off</div>
                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-pizza-slice"></i>Pizza</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Samusage pasta lit</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                                <div class="price">$26.08 <del>$32.5</del></div>
                                                <a href="cart.html" class="bttn-small btn-fill">Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-four" role="tabpanel" aria-labelledby="tab-four-tab">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/items/21.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href="" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-heart"></i></a></div>
                                            <div class="ratings">4.6</div>
                                            <div class="discount-rate">10% off</div>
                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-map-marker-alt"></i>Los Angels</a>
                                                <a href=""><i class="fas fa-utensils"></i>Kolas Food</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Samusage pasta lit</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                                <div class="price">$26.08 <del>$32.5</del></div>
                                                <a href="cart.html" class="bttn-small btn-fill">Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/items/22.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href="" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-heart"></i></a></div>
                                            <div class="ratings">4.6</div>
                                            <div class="discount-rate">10% off</div>
                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-map-marker-alt"></i>Los Angels</a>
                                                <a href=""><i class="fas fa-utensils"></i>Kolas Food</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Samusage pasta lit</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                                <div class="price">$26.08 <del>$32.5</del></div>
                                                <a href="cart.html" class="bttn-small btn-fill">Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/items/23.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href=""><i class="fas fa-heart"></i></a></div>
                                            <div class="ratings">4.6</div>
                                            <div class="discount-rate">10% off</div>
                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-map-marker-alt"></i>Los Angels</a>
                                                <a href=""><i class="fas fa-utensils"></i>Kolas Food</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Samusage pasta lit</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                                <div class="price">$26.08 <del>$32.5</del></div>
                                                <a href="cart.html" class="bttn-small btn-fill">Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/items/24.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href=""><i class="fas fa-heart"></i></a></div>
                                            <div class="ratings">4.6</div>
                                            <div class="discount-rate">10% off</div>
                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-map-marker-alt"></i>Los Angels</a>
                                                <a href=""><i class="fas fa-utensils"></i>Kolas Food</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Samusage pasta lit</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                                <div class="price">$26.08 <del>$32.5</del></div>
                                                <a href="cart.html" class="bttn-small btn-fill">Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-five" role="tabpanel" aria-labelledby="tab-five-tab">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/items/25.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href="" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-heart"></i></a></div>
                                            <div class="ratings">4.6</div>
                                            <div class="discount-rate">10% off</div>
                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-map-marker-alt"></i>Los Angels</a>
                                                <a href=""><i class="fas fa-utensils"></i>Kolas Food</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Samusage pasta lit</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                                <div class="price">$26.08 <del>$32.5</del></div>
                                                <a href="cart.html" class="bttn-small btn-fill">Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/items/26.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href="" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-heart"></i></a></div>
                                            <div class="ratings">4.6</div>
                                            <div class="discount-rate">10% off</div>
                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-map-marker-alt"></i>Los Angels</a>
                                                <a href=""><i class="fas fa-utensils"></i>Kolas Food</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Samusage pasta lit</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                                <div class="price">$26.08 <del>$32.5</del></div>
                                                <a href="cart.html" class="bttn-small btn-fill">Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/items/27.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href=""><i class="fas fa-heart"></i></a></div>
                                            <div class="ratings">4.6</div>
                                            <div class="discount-rate">10% off</div>
                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-map-marker-alt"></i>Los Angels</a>
                                                <a href=""><i class="fas fa-utensils"></i>Kolas Food</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Samusage pasta lit</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                                <div class="price">$26.08 <del>$32.5</del></div>
                                                <a href="cart.html" class="bttn-small btn-fill">Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/items/28.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href=""><i class="fas fa-heart"></i></a></div>
                                            <div class="ratings">4.6</div>
                                            <div class="discount-rate">10% off</div>
                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-map-marker-alt"></i>Los Angels</a>
                                                <a href=""><i class="fas fa-utensils"></i>Kolas Food</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Samusage pasta lit</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                                <div class="price">$26.08 <del>$32.5</del></div>
                                                <a href="cart.html" class="bttn-small btn-fill">Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-six" role="tabpanel" aria-labelledby="tab-six-tab">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/items/29.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href="" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-heart"></i></a></div>
                                            <div class="ratings">4.6</div>
                                            <div class="discount-rate">10% off</div>
                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-map-marker-alt"></i>Los Angels</a>
                                                <a href=""><i class="fas fa-utensils"></i>Kolas Food</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Samusage pasta lit</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                                <div class="price">$26.08 <del>$32.5</del></div>
                                                <a href="cart.html" class="bttn-small btn-fill">Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/items/30.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href="" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-heart"></i></a></div>
                                            <div class="ratings">4.6</div>
                                            <div class="discount-rate">10% off</div>
                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-map-marker-alt"></i>Los Angels</a>
                                                <a href=""><i class="fas fa-utensils"></i>Kolas Food</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Samusage pasta lit</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                                <div class="price">$26.08 <del>$32.5</del></div>
                                                <a href="cart.html" class="bttn-small btn-fill">Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/items/31.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href=""><i class="fas fa-heart"></i></a></div>
                                            <div class="ratings">4.6</div>
                                            <div class="discount-rate">10% off</div>
                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-map-marker-alt"></i>Los Angels</a>
                                                <a href=""><i class="fas fa-utensils"></i>Kolas Food</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Samusage pasta lit</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                                <div class="price">$26.08 <del>$32.5</del></div>
                                                <a href="cart.html" class="bttn-small btn-fill">Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                                    <div class="single-listed-food">
                                        <div class="img">
                                            <a href=""><img src="{{ asset('assets/images/items/32.jpg') }}" alt=""></a>
                                            <div class="wishlist"><a href=""><i class="fas fa-heart"></i></a></div>
                                            <div class="ratings">4.6</div>
                                            <div class="discount-rate">10% off</div>
                                        </div>
                                        <div class="item-details">
                                            <div class="restaurant-name-location">
                                                <a href=""><i class="fas fa-map-marker-alt"></i>Los Angels</a>
                                                <a href=""><i class="fas fa-utensils"></i>Kolas Food</a>
                                            </div>
                                            <div class="title">
                                                <h3><a href="">Samusage pasta lit</a></h3>
                                            </div>
                                            <div class="des">
                                                <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                            </div>
                                            <div class="price-ratings">
                                                <div class="price">$26.08 <del>$32.5</del></div>
                                                <a href="cart.html" class="bttn-small btn-fill">Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/Food Category-->

    <!-- Modal for wishlist -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Login Your Account</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="account-form">
                        <div class="via-login">
                            <a href="" class="facebook-bg"><i class="fab fa-facebook-f"></i></a>
                            <a href="" class="google-plus-bg"><i class="fab fa-google"></i></a>
                            <a href="" class="linkedin-bg"><i class="fab fa-linkedin-in"></i></a>
                        </div>
                        <form action="#">
                            <div class="row">
                                <div class="col-xl-12">
                                    <input type="text" placeholder="Username" required>
                                </div>
                                <div class="col-xl-12">
                                    <input type="password" placeholder="Password" required>
                                </div>

                                <div class="col-xl-12">
                                    <button type="submit" class="bttn-mid btn-fill w-100">Login Account</button>
                                </div>
                                <div class="col-xl-12">
                                    <p><a href="forgot-password.html">Forgot password</a> | <a href="register.html">Create account</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /Modal for wishlist -->


    <!-- Counter Area -->
    <section class="section-padding-2 dark-overlay" style="background: url('https://127.0.0.1:8000/assets/images/banner-2.jpg') no-repeat fixed;">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="single-new-counter">
                        <img src="assets/images/icons/8.png" alt="">
                        <h3 class="count">5246</h3>
                        <h5>Food Sold</h5>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="single-new-counter">
                        <img src="assets/images/icons/9.png" alt="">
                        <h3 class="count">2001</h3>
                        <h5>Since we're</h5>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="single-new-counter">
                        <img src="assets/images/icons/10.png" alt="">
                        <h3 class="count">68</h3>
                        <h5>Menu Items</h5>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="single-new-counter">
                        <img src="assets/images/icons/11.png" alt="">
                        <h3>$<span class="count">11.5</span>M</h3>
                        <h5>Total sell</h5>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /Counter Area -->


    <!-- Popular Items -->
    <section class="section-padding-2">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="section-title">
                        <h4>Best Selling</h4>
                        <h2>Nos meilleures Produits</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="popular-items owl-carousel">
                        <div class="single-listed-food">
                            <div class="img">
                                <a href=""><img src="assets/images/items/1.jpg" alt=""></a>
                                <div class="wishlist"><a href="" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-heart"></i></a></div>
                            </div>
                            <div class="item-details">
                                <div class="restaurant-name-location">
                                    <a href=""><i class="fas fa-hamburger"></i>Burger</a>
                                </div>
                                <div class="title">
                                    <h3><a href="">Samusage pasta lit</a></h3>
                                </div>
                                <div class="des">
                                    <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                </div>
                                <div class="price-ratings">

                                </div>
                            </div>
                        </div>
                        <div class="single-listed-food">
                            <div class="img">
                                <a href=""><img src="assets/images/items/1.jpg" alt=""></a>
                                <div class="wishlist"><a href="" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-heart"></i></a></div>
                            </div>
                            <div class="item-details">
                                <div class="restaurant-name-location">
                                    <a href=""><i class="fas fa-pizza-slice"></i>Pizza</a>
                                </div>
                                <div class="title">
                                    <h3><a href="">Samusage pasta lit</a></h3>
                                </div>
                                <div class="des">
                                    <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                </div>
                                <div class="price-ratings">
                                </div>
                            </div>
                        </div>
                        <div class="single-listed-food">
                            <div class="img">
                                <a href=""><img src="assets/images/items/2.jpg" alt=""></a>
                                <div class="wishlist"><a href=""><i class="fas fa-heart"></i></a></div>
                            </div>
                            <div class="item-details">
                                <div class="restaurant-name-location">
                                    <a href=""><i class="fas fa-pizza-slice"></i>Pizza</a>
                                </div>
                                <div class="title">
                                    <h3><a href="">Samusage pasta lit</a></h3>
                                </div>
                                <div class="des">
                                    <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                </div>
                                <div class="price-ratings">
                                </div>
                            </div>
                        </div>
                        <div class="single-listed-food">
                            <div class="img">
                                <a href=""><img src="assets/images/items/3.jpg" alt=""></a>
                                <div class="wishlist"><a href=""><i class="fas fa-heart"></i></a></div>

                            </div>
                            <div class="item-details">
                                <div class="restaurant-name-location">
                                    <a href=""><i class="fas fa-pizza-slice"></i>Pizza</a>
                                </div>
                                <div class="title">
                                    <h3><a href="">Samusage pasta lit</a></h3>
                                </div>
                                <div class="des">
                                    <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                </div>
                                <div class="price-ratings">

                                </div>
                            </div>
                        </div>
                        <div class="single-listed-food">
                            <div class="img">
                                <a href=""><img src="assets/images/items/2.jpg" alt=""></a>
                                <div class="wishlist"><a href=""><i class="fas fa-heart"></i></a></div>

                            </div>
                            <div class="item-details">
                                <div class="restaurant-name-location">
                                    <a href=""><i class="fas fa-pizza-slice"></i>Pizza</a>
                                </div>
                                <div class="title">
                                    <h3><a href="">Samusage pasta lit</a></h3>
                                </div>
                                <div class="des">
                                    <p>Wibusdam voluptates quas velit quis dignissimos iure at asperiores delectus dicta blanditiis</p>
                                </div>
                                <div class="price-ratings">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /Popular Items -->

    <!--CTA Section-->

    <!--User Review Section-->
    <section class="section-padding-2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8 centered">
                    <div class="section-title">
                        <h4>What our client says</h4>
                        <h2>Avis de clients</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-sm-6">
                    <div class="single-user-review">
                        <div class="quote-icon">
                            <img src="assets/images/avis1.png" alt="">
                        </div>
                        <div class="review">
                            <p>J'adore tous ses produits ils sont très bons</p>
                        </div>
                        <div class="reviewer-thumb">
                            <img src="assets/images/avis1.jpg" alt="">
                            <p>Ouma A'yr</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-sm-6">
                    <div class="single-user-review">
                        <div class="quote-icon">
                            <img src="assets/images/avis2.png" alt="">
                        </div>
                        <div class="review">
                            <p>Vraiment une bonne qualité dans tous ces produits alors bravo</p>
                        </div>
                        <div class="reviewer-thumb">
                            <img src="assets/images/avis2.jpg" alt="">
                            <p>Noura Slimani</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-sm-6">
                    <div class="single-user-review">
                        <div class="quote-icon">
                            <img src="assets/images/avis3.png" alt="">
                        </div>
                        <div class="review">
                            <p>Bon rapport prix qualité Bonne continuation</p>
                        </div>
                        <div class="reviewer-thumb">
                            <img src="assets/images//avis3.jpg" alt="">
                            <p>Kawthar Kraimi Thabet</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/User Review Section-->

    <!--Reservation Section-->
    <section class="section-padding gray-bg flower">
        <div class="container">
            <div class="row">
                <div class="col centered">
                    <div class="section-title">
                        <h4>Contactez nous</h4>
                        <h2>Contactez nous</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-10">
                    <div class="reservation-form">
                        <form action="#">
                            <div class="row">
                                <div class="col-xl-6 col-md-6">
                                    <div class="single-box">
                                        <label for="name">Name</label>
                                        <input type="text" id="name">
                                    </div>
                                </div>
                                <div class="col-xl-6 col-md-6">
                                    <div class="single-box">
                                        <label for="email">Email</label>
                                        <input type="email" id="email">
                                    </div>
                                </div>


                                <div class="col-xl-12 col-md-12">
                                    <div class="single-box">
                                        <label for="msg">Your Message</label>
                                        <textarea name="msg" rows="5" id="msg"></textarea>
                                    </div>
                                </div>
                                <div class="col-xl-12 col-md-12">
                                    <button class="bttn-mid btn-fill">Envoyer</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/Reservation Section-->

    <!--newslatter and Instagram feed-->
    <div class="section-padding-2">
        <div class="container-fluid">
            <div class="row justify-content-center mb-30">
                <div class="col-xl-12 centered">
                    <div class="section-title">
                        <h4>Follow us <a href="" class="cl-primary">@Instagram</a></h4>
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="photo-gallery owl-carousel">
                        <div class="single-insta">
                            <a href="">
                                <img src="{{ asset('assets/images/insta/1.jpg') }}" alt="">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </div>
                        <div class="single-insta">
                            <a href="">
                                <img src="{{ asset('assets/images/insta/2.jpg') }}" alt="">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </div>
                        <div class="single-insta">
                            <a href="">
                                <img src="{{ asset('assets/images/insta/3.jpg') }}" alt="">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </div>
                        <div class="single-insta">
                            <a href="">
                                <img src="{{ asset('assets/images/insta/4.jpg') }}" alt="">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </div>
                        <div class="single-insta">
                            <a href="">
                                <img src="{{ asset('assets/images/insta/5.jpg') }}" alt="">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </div>
                        <div class="single-insta">
                            <a href="">
                                <img src="{{ asset('assets/images/insta/6.jpg') }}" alt="">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-5 col-lg-5 col-sm-12 centered">
                    <div class="newslatter">
                        <form action="#">
                            <input type="email" placeholder="Email Address" required>
                            <button type="submit">Subscribe</button>
                        </form>
                        <div class="social">
                            <a href=""><i class="fab fa-facebook-square"></i></a>
                            <a href=""><i class="fab fa-instagram-square"></i></a>
                            <a href=""><i class="fab fa-twitter-square"></i></a>
                            <a href=""><i class="fab fa-pinterest-square"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/newslatter and Instagram feed-->

@endsection
