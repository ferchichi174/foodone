<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">


    <link rel="shortcut icon" href="{{ asset('assets/images/1.png') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('assets/images/1.png') }}" type="image/x-icon">

    <title>Foodone Tunisie | Foodone</title>

    <!-- Bootstrap -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/css/fontawesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/magnific-popup.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/jquery-ui.css') }}" rel="stylesheet">


    <link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/owl.carousel.min.css') }}" rel="stylesheet">


    <!-- Main css -->
    <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<!-- Preloader -->
<div class="preloader">
    <img src="{{ asset('assets/images/1.png') }}" alt="">
</div><!--/Preloader -->

<!--Header Area-->
<header class="header-area">
    <nav class="navbar navbar-expand-lg main-menu">
        <div class="container-fluid">

            <a class="navbar-brand" href="index.html"><img src="{{ asset('assets/images/1.png') }}" class="d-inline-block align-top" alt=""></a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="menu-toggle"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav m-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="" role="button" data-toggle="dropdown" aria-expanded="false">Home</a>
                    </li>
                    <!-- <li class="nav-item"><a class="nav-link" href="shop.html">Shop</a></li> -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="" role="button" data-toggle="dropdown" aria-expanded="false">Qui sommes-nous ?</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="" role="button" data-toggle="dropdown" aria-expanded="false">Produits</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="about.html">Recette</a></li>
                    <li class="nav-item"><a class="nav-link" href="gallery.html">Contact</a></li>

                </ul>
                <div class="header-btn justify-content-end">

                </div>
            </div>
        </div>
    </nav>
</header><!--/Header Area-->

<!-- Banner Area-->

<div class="hero owl-carousel flower">
    @yield('carousel')

</div><!-- /Banner Area-->

@yield('content')
<!--About Area-->

<!--Footer Area-->
<footer class="footer-area section-padding light-primary-bg">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-md-4 col-sm-6">
                <div class="footer-widget">
                    <h3>About Foodzza</h3>
                    <p>Quia aliquam consequatur, aperiam at totam eaque aliquid. Deleniti magni ducimus libero modi nobis, doloremque in architecto</p>
                    <div class="socials">
                        <a href="" class="facebook-bg"><i class="fab fa-facebook-f"></i></a>
                        <a href="" class="instagram-bg"><i class="fab fa-instagram"></i></a>
                        <a href="" class="youtube-bg"><i class="fab fa-youtube"></i></a>
                        <a href="" class="twitter-bg"><i class="fab fa-twitter"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-md-8 col-sm-12">
                <div class="row">
                    <div class="col-xl-4 col-md-4 col-sm-6">
                        <div class="footer-widget">
                            <h3>Overview</h3>
                            <div class="footer-nav">
                                <ul>
                                    <li><a href="">How it works</a></li>
                                    <li><a href="">About us</a></li>
                                    <li><a href="">Career</a></li>
                                    <li><a href="">Be a partnar</a></li>
                                    <li><a href="">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-4 col-sm-6">
                        <div class="footer-widget">
                            <h3>Solution</h3>
                            <div class="footer-nav">
                                <ul>
                                    <li><a href="">Breakfast</a></li>
                                    <li><a href="">Lunch</a></li>
                                    <li><a href="">Dinner</a></li>
                                    <li><a href="">Snacks</a></li>
                                    <li><a href="">Coffee</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-4 col-sm-6">
                        <div class="footer-widget">
                            <h3>Resource</h3>
                            <div class="footer-nav">
                                <ul>
                                    <li><a href="">Restaurant Listing</a></li>
                                    <li><a href="">Coupon</a></li>
                                    <li><a href="">Blog</a></li>
                                    <li><a href="">Faq</a></li>
                                    <li><a href="">Checkout</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row copyright">
            <div class="col-xl-6 col-sm-6">
                <div class="copyright-text">
                    <p>Copyright &copy;2021. All Rights Reserved.</p>
                </div>
            </div>
            <div class="col-xl-6 col-sm-6">
                <div class="copyright-icon">
                    <img src="{{ asset('assets/images/payment.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</footer><!--/Footer Area-->



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="assets/js/jquery-3.2.1.min.js"></script>
<script src="assets/js/jquery-migrate.js"></script>
<script src="assets/js/jquery-ui.js"></script>

<script src="assets/js/popper.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>

<script src="assets/js/magnific-popup.min.js"></script>
<script src="assets/js/imagesloaded.pkgd.min.js"></script>
<script src="assets/js/isotope.pkgd.min.js"></script>

<script src="assets/js/waypoints.min.js"></script>
<script src="assets/js/jquery.counterup.min.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/scrollUp.min.js"></script>

<script src="assets/js/cartsidebar.js"></script>
<script src="assets/js/script.js"></script>

</body>
</html>
